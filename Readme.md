

# Learning k8s by example


## References

1.  docsite <https://kubernetes.io/docs/home/>
2.  Cloud specific docs
    -   <https://docs.aws.amazon.com/eks/latest/userguide/getting-started.html>
    -   <https://cloud.google.com/kubernetes-engine/docs/quickstart>
3.  Cheat Sheet <https://kubernetes.io/docs/reference/kubectl/cheatsheet/>


## Resources: summary


### Unit of Isolation

-   Namespace


### Configuration

-   ConfigMap
-   Secret


### Workloads

-   Job & CronJob
-   Deployment & Replicaset
-   StatefulSet
-   DaemonSet
-   Pod


### Network / Traffic shaping

-   Service
    1.  ClusterIP
    2.  LoadBalancer
    3.  Headless
    4.  NodePort
-   Ingress
-   EndpointSlice
-   NetworkPolicy


### Storage

-   Mount
-   PV
-   PVC


### Other

-   CRD


## Using the right resources.

-   Try not to use Pods directly.
-   abstraction that you want probably already exists


## Meta Schema

      apiVersion: apps/v1
      kind: TypeOfResource
      metadata:
        name: name of the rersource
        labels:
          someKey: someBalue
      spec:
      ...
    ---


## Kind / Minikube


### Kind

<https://kind.sigs.k8s.io/docs/user/quick-start/>    


### Minikube

<https://kubernetes.io/docs/tutorials/hello-minikube/>


## Hello World


### Code

<https://github.com/irfn/hello-web>
<https://github.com/irfn/hello-web/blob/master/main.go>


### Docker Image


### Architecture


### Resources

-   what resources would you use?
-   what is the upgrade strategy


### with kubectl and resources


### with helm


## Deploy some open source Application


### using helm charts


### DIY


## Transcript

Hello world Example

    #install either Minikube or Kind
    brew install minikube
    #install kubectl
    
    brew install kubectl
    #install helm
    brew install helm
    minikube status
    minikube start
    
    kubectx
    
    #create a namespace.
    kubectl create namespace setu-test
    
    #switch to newly created namespace
    kubens
    
    #Add some resource definitions to a yaml
    cat hello/hello.yaml
    
    kubectl apply -f hello/hello.yaml
    kubectl get pods
    kubectl logs -f pods/hello-web-79cdf8d576-k7m8l
    
    kubectl port-forward po/hello-web-79cdf8d576-k7m8l 8080:80
    kubectl exec -it pod/hello-web-79cdf8d576-k7m8l sh
    
    curl localhost:8080/irfn
    curl localhost:8080

Helm example

    helm repo add enix https://charts.enix.io/
    helm install default-netbox enix/netbox
    helm ls
    kubectl get all
    kubectl port-forward service/default-netbox 8080:80

